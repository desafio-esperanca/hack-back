from django.contrib.auth import authenticate
from django.shortcuts import redirect, reverse
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from rest_framework.response import Response
from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.authtoken.models import Token
from .serializers import *
from .models import *


class LoginView(APIView):

     def post(self, request):
        email = self.request.data.get('email')
        password = self.request.data.get('password')
        try:
            user = User.objects.get(email=email)
        except:
            return Response({})

        user = authenticate(username=user.username, password=password)
        if user is not None:
            return redirect(reverse('account:user_detail', kwargs={'pk': user.id}))
            return Response({'msg': 'Logged in!', 'id': user.userprofile.id})
        else:
            return Response({})

class TeamUpView(APIView):

     def post(self, request):
        user_id = int(self.request.data.get('user_id'))
        team_id = int(self.request.data.get('team_id'))
        try:
            user = UserProfile.objects.get(id=user_id)
            team = Team.objects.get(id=team_id)
        except:
            return Response({'msg': False})

        user.team = team
        user.save()
        
        return Response({'msg': True})
        


class UserProfileDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = UserProfile.objects.all()
    serializer_class = UserProfileSerializer

class UserProfileList(generics.ListCreateAPIView):
    queryset = UserProfile.objects.all()
    serializer_class = UserProfileSerializer

    def perform_create(self, serializer):
        first_name = self.request.data.get('first_name')
        last_name = self.request.data.get('last_name')
        username = self.request.data.get('username')
        email = self.request.data.get('email')
        password = self.request.data.get('password')

        user = User.objects.create_user(username, email, password)
        user.first_name = first_name
        user.last_name = last_name
        user.save()
        serializer.save(user=user)

        Token.objects.create(user=user)

class TeamDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Team.objects.all()
    serializer_class = TeamSerializer

class TeamList(generics.ListCreateAPIView):
    queryset = Team.objects.all()
    serializer_class = TeamSerializer


class CompanyDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Company.objects.all()
    serializer_class = CompanySerializer

class CompanyList(generics.ListCreateAPIView):
    queryset = Company.objects.all()
    serializer_class = CompanySerializer
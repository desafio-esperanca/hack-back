from rest_framework import serializers
from .models import *
from challenge.models import TeamCompletedChallenge, UserCompletedChallenge
import datetime as dt
from django.db.models import Sum


class TeamSerializer(serializers.ModelSerializer):
    score = serializers.SerializerMethodField('add_score')
    leader = serializers.SerializerMethodField('add_leader')
    classification = serializers.SerializerMethodField('add_classification')

    def add_score(self, obj):
        today = dt.datetime.today()
        team_score = TeamCompletedChallenge.objects.select_related('challenge').filter(
            team=obj,
            challenge__end_date__isnull=False,
            challenge__end_date__gt=today
        ).aggregate(score=Sum('challenge__points'))

        user_score = UserCompletedChallenge.objects.select_related('challenge').filter(
            user__team=obj,
            challenge__end_date__isnull=False,
            challenge__end_date__gt=today
        ).aggregate(score=Sum('challenge__points'))

        team_score = team_score['score'] if team_score['score'] is not None else 0
        user_score = user_score['score'] if user_score['score'] is not None else 0
        return team_score + user_score
    
    def add_classification(self, obj):
        return sorted([{'image_url': team.image_url,'name': team.name, 'id':team.id,'score': self.add_score(team) } for team in Team.objects.all()], key=lambda x: x['score'], reverse=True)

    def add_leader(self, obj):
        return {
            'first_name': obj.leader.user.first_name,
            'last_name': obj.leader.user.last_name,
            'id': obj.leader.id,
        } if obj.leader else None

    class Meta:
        model = Team
        fields = ('name', 'score', 'leader', 'classification', 'id', 'image_url', 'live_url')


class UserProfileSerializer(serializers.ModelSerializer):
    user_info = serializers.SerializerMethodField('add_user_info')
    user_type_name = serializers.SerializerMethodField('add_user_type')
    team = TeamSerializer(required=False, allow_null=True)
    score = serializers.SerializerMethodField('add_score')
    classification = serializers.SerializerMethodField('add_classification')

    def add_user_info(self, obj):
        return {
            'first_name': obj.user.first_name,
            'last_name': obj.user.last_name,
            'email': obj.user.email,
        }

    def add_user_type(self, obj):
        return UserProfile.ChallengeType.get_choice(obj.user_type).label

    def add_score(self, obj):
        today = dt.datetime.today()
        score = UserCompletedChallenge.objects.select_related('challenge').filter(
            user=obj,
            challenge__end_date__isnull=False,
            challenge__end_date__gt=today
        ).aggregate(score=Sum('challenge__points'))
        return score['score'] if score['score'] is not None else 0

    def add_classification(self, obj):
        return sorted([{'image_url': u.image_url, 'name': u.user.first_name +' ' +u.user.last_name, 'id': u.id, 'score': self.add_score(u)} for u in UserProfile.objects.filter(team__isnull=False, user_type=UserProfile.ChallengeType.player,)], key=lambda x: x['score'], reverse=True)

    class Meta:
        model = UserProfile
        fields = ('user_info', 'team', 'user_type', 'user_type_name', 'id', 'score', 'classification', 'image_url')


class CompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = '__all__'
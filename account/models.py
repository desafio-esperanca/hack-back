from django.db import models
from django.contrib.auth.models import User
from djchoices import ChoiceItem, DjangoChoices


class UserProfile(models.Model):
    class ChallengeType(DjangoChoices):
        player = ChoiceItem(0, 'Player')
        influencer = ChoiceItem(1, 'Influencer')

    user = models.OneToOneField(User, blank=True, null=True, on_delete=models.CASCADE)
    team = models.ForeignKey('Team', related_name='team', blank=True, null=True, on_delete=models.CASCADE)
    created_date = models.DateTimeField(auto_now_add=True)
    last_login_date = models.DateTimeField(auto_now=True)
    user_type = models.PositiveSmallIntegerField(choices=ChallengeType.choices)
    image_url = models.URLField(blank=True, null=True)
    
    def __str__(self):
        return self.user.email

class Team(models.Model):
    name = models.CharField(max_length=50)
    image_url = models.URLField(blank=True, null=True)
    leader = models.ForeignKey('UserProfile', related_name='leader', on_delete=models.CASCADE, blank=True, null=True)
    live_url = models.URLField(blank=True, null=True)

    def __str__(self):
        return self.name

class Company(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name
    
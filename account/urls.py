from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from . import views


urlpatterns = [

    url(r'login/?$', views.LoginView.as_view(), name='login'),
    url(r'teamup/?$', views.TeamUpView.as_view(), name='teamup'),

    url(r'user/(?P<pk>[0-9]+)/?$', views.UserProfileDetail.as_view(), name='user_detail'),
    url(r'user/?$', views.UserProfileList.as_view(), name='user_list'),
    
    url(r'team/(?P<pk>[0-9]+)/?$', views.TeamDetail.as_view(), name='team_detail'),
    url(r'team/?$', views.TeamList.as_view(), name='team_list'),

    url(r'company/(?P<pk>[0-9]+)/?$', views.CompanyDetail.as_view(), name='company_detail'),
    url(r'company/?$', views.CompanyList.as_view(), name='company_list'),

]

urlpatterns = format_suffix_patterns(urlpatterns)

from django.db import models
from account.models import Company, Team, UserProfile
from djchoices import ChoiceItem, DjangoChoices


class Season(models.Model):
    name = models.CharField(max_length=50)
    created_date = models.DateTimeField(auto_now_add=True)
    end_date = models.DateField(blank=True, null=True)

    def __str__(self):
        return self.name

class Challenge(models.Model):
    class ChallengeType(DjangoChoices):
        player = ChoiceItem(0, 'Player')
        team = ChoiceItem(1, 'Team')
        influencer = ChoiceItem(2, 'Influencer')
    
    def __str__(self):
        return self.name

    name = models.CharField(max_length=80)
    image_url = models.URLField(blank=True, null=True)
    sponsor = models.ForeignKey(Company, on_delete=models.CASCADE, blank=True, null=True)
    season = models.ForeignKey(Season, on_delete=models.CASCADE, blank=True, null=True)
    objective = models.TextField(blank=True, null=True)
    points = models.PositiveSmallIntegerField()
    challenge_type = models.PositiveSmallIntegerField(choices=ChallengeType.choices)
    created_date = models.DateTimeField(auto_now_add=True)
    end_date = models.DateField(blank=True, null=True)

class TeamCompletedChallenge(models.Model):
    challenge = models.ForeignKey(Challenge, on_delete=models.CASCADE)
    team = models.ForeignKey(Team, on_delete=models.CASCADE)
    completed_date = models.DateTimeField(auto_now_add=True, blank=True, null=True)

class UserCompletedChallenge(models.Model):
    challenge = models.ForeignKey(Challenge, on_delete=models.CASCADE)
    user = models.ForeignKey(UserProfile, on_delete=models.CASCADE)
    completed_date = models.DateTimeField(auto_now_add=True, blank=True, null=True)
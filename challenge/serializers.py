from rest_framework import serializers
from .models import *


class SeasonSerializer(serializers.ModelSerializer):
    class Meta:
        model = Season
        fields = '__all__'

class ChallengeSerializer(serializers.ModelSerializer):
    sponsor = serializers.SerializerMethodField('add_sponsor')
    challenge_type = serializers.SerializerMethodField('add_challenge_type')
    season = serializers.SerializerMethodField('add_season')

    def add_sponsor(self, obj):
        return {
            'name': obj.sponsor.name,
            'id': obj.sponsor.id
        } if obj.sponsor else None
    
    def add_challenge_type(self, obj):
        return Challenge.ChallengeType.get_choice(obj.challenge_type).label
    
    def add_season(self, obj):
        return {
            'name': obj.season.name,
            'id': obj.season.id
        } if obj.season else None

    class Meta:
        model = Challenge
        fields = ('name', 'season', 'sponsor', 'objective', 'points', 'created_date', 'end_date', 'challenge_type')

class TeamCompletedChallengeSerializer(serializers.ModelSerializer):
    challenge = ChallengeSerializer()

    class Meta:
        model = TeamCompletedChallenge
        fields = '__all__'

class UserCompletedChallengeSerializer(serializers.ModelSerializer):
    challenge = ChallengeSerializer()
    
    class Meta:
        model = UserCompletedChallenge
        fields = '__all__'
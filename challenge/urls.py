from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from . import views


urlpatterns = [

    url(r'season/(?P<pk>[0-9]+)/?$', views.SeasonDetail.as_view(), name='season_detail'),
    url(r'season/?$', views.SeasonList.as_view(), name='season_list'),
    
    url(r'challenge/(?P<pk>[0-9]+)/?$', views.ChallengeDetail.as_view(), name='challenge_detail'),
    url(r'challenge/?$', views.ChallengeList.as_view(), name='challenge_list'),

    url(r'trophies/(?P<user_id>[0-9]+)/?$', views.TrophiesView.as_view(), name='trophies'),

]

urlpatterns = format_suffix_patterns(urlpatterns)

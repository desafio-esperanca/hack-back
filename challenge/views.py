from django.shortcuts import render
from rest_framework.response import Response
from rest_framework import generics
from rest_framework.views import APIView
from .serializers import *
from .models import *
from account.serializers import UserProfileSerializer


class SeasonDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Season.objects.all()
    serializer_class = SeasonSerializer

class SeasonList(generics.ListCreateAPIView):
    queryset = Season.objects.all()
    serializer_class = SeasonSerializer


class ChallengeDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Challenge.objects.all()
    serializer_class = ChallengeSerializer

class ChallengeList(generics.ListCreateAPIView):
    queryset = Challenge.objects.all()
    serializer_class = ChallengeSerializer


class TeamCompletedChallengeDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = TeamCompletedChallenge.objects.all()
    serializer_class = TeamCompletedChallengeSerializer

class TeamCompletedChallengeList(generics.ListCreateAPIView):
    queryset = TeamCompletedChallenge.objects.all()
    serializer_class = TeamCompletedChallengeSerializer


class UserCompletedChallengeDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = UserCompletedChallenge.objects.all()
    serializer_class = UserCompletedChallengeSerializer

class UserCompletedChallengeList(generics.ListCreateAPIView):
    queryset = UserCompletedChallenge.objects.all()
    serializer_class = UserCompletedChallengeSerializer

class TrophiesView(APIView):

    def get(self, request, user_id):
        user = UserProfile.objects.get(id=user_id)
        user_completed = UserCompletedChallenge.objects.select_related('challenge').filter(user=user)
        team_completed = TeamCompletedChallenge.objects.select_related('challenge').filter(team=user.team)

        serializer_class = UserCompletedChallengeSerializer(user_completed, many=True)
        serialized_data = serializer_class.data

        serializer_class = TeamCompletedChallengeSerializer(team_completed, many=True)
        serialized_data.extend(serializer_class.data)
        serialized_data = sorted(serialized_data, key=lambda x: x['completed_date'], reverse=True)
        
        trophies_data = {
            'timeline': serialized_data,
            'score': UserProfileSerializer().add_score(user)
        }
        return Response(trophies_data)

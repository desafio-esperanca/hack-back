# Generated by Django 2.0.4 on 2018-04-28 23:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('challenge', '0002_challenge_end_date'),
    ]

    operations = [
        migrations.AddField(
            model_name='season',
            name='end_date',
            field=models.DateField(blank=True, null=True),
        ),
    ]

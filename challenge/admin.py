from django.contrib import admin
from .models import *


@admin.register(Season)
class SeasonAdmin(admin.ModelAdmin):
    list_display = ('name',)

@admin.register(Challenge)
class ChallengeAdmin(admin.ModelAdmin):
    list_display = ('name',)

@admin.register(UserCompletedChallenge)
class UserCompletedChallengeAdmin(admin.ModelAdmin):
    pass

@admin.register(TeamCompletedChallenge)
class CompanyAdmin(admin.ModelAdmin):
    pass